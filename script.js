var game = {
	boardSize : 7,
	numShips : 3,
	shipLength : 3,
	shipsSunk : 0,

	ships : [	
					{ 
						locations: [0, 0, 0], 
						hits: ["", "", ""] 
					},
					{ 
						locations: [0, 0, 0], 
						hits: ["", "", ""] 
					},
					{ 
						locations: [0, 0, 0], 
						hits: ["", "", ""] 
					}
				],

	fire : 	function(guess) {
					for (var i = 0; i < this.numShips; i++) {
						var ship = this.ships[i];
						var index = ship.locations.indexOf(guess);

						if (index >= 0) {
							ship.hits[index] = "hit";
							view.outputImage(guess);
							view.outputMessage('HIT!');

							if (this.isSunk(ship)) {
								view.outputMessage('U sank my ship!');
								this.shipsSunk++;
							}
							return true;
						}
					}
					view.outputImage(guess);
					view.outputMessage('MISS!');
					return false;
				},

	isSunk : function(ship) {
					for (var i = 0; i < this.shipLength; i++) {
						if (ship.hits[i] !== "hit") { return false; }
					}
					return true;
				},

	generateShipLocations : function() {
										var locations;

										for (var i = 0; i < this.numShips; i++) {
											do {
												locations = this.generateShip();
											} while (this.collision(locations))

											this.ships[i].locations = locations;
										}
									},

	generateShip : function() { 
							var direction = Math.floor(Math.random() * 2);
							var row, col;

							if (direction === 1)  { //ship = ---
								row = Math.floor(Math.random() * this.boardSize);
								col = row = Math.floor(Math.random() * (this.boardSize - this.shipLength));
							} else { //ship = |
								row = Math.floor(Math.random() * (this.boardSize - this.shipLength));
								col = row = Math.floor(Math.random() * this.boardSize);
							}

							var newShipLocations = [];
							for (var i = 0; i < this.shipLength; i++) {
								if (direction === 1)  { //ship = ---
									newShipLocations.push(row + "" + (col+i));
								} else { //ship = |
									newShipLocations.push((row+i) + "" + col);
								}
							}
							return newShipLocations;
						},

	collision : function() {
						for (var i = 0; i < this.numShips; i++) {
							var ship = game.ships[i];
							for (var j = 0; j < location.length; j++) {
								if (ship.locations.indexOf(locations[j]) >= 0) {}
							}
						}
						return false;
					}
};

var controller = {
	guesses : 0,

	processGuess : function(guess) {
		var location = parseGuess(guess);

		if(location) {
			this.guesses++;
			var hit = game.fire(location);

			if (hit && game.shipsSunk === game.numShips) {
				view.outputMessage("U sank all my ships, in " + this.guesses + " guesses");
				alert("You win! Congratulations!");
			}
		}
	}
};

function parseGuess(guess) {
	var alphabet = ["A", "B", "C", "D", "E", "F", "G"];

	if (guess === null || guess.length !== 2) {
		alert("Your coordinate isn't valid, please enter a valid coordinate...");
	} else {
		firstChar = guess.charAt(0);
		var row = alphabet.indexOf(firstChar);
		var column = guess.charAt(1);
							
		if (isNaN(row) || isNaN(column)) {
			alert("This coordinate isn't on the board, please try again...");
		} else if (row < 0 || column < 0 || row >= game.boardSize || column >= game.boardSize) {
			alert("Your coordinate off the board, please try again...");
		} else {
			return row + column;
		}
	}
	return null;
}

function init() {
	var buttonValue = document.getElementById('button');
	buttonValue.onclick = handleClickButton;
	var guessInput = document.getElementById('guess');
	guessInput.onkeypress = handleKeyPress; 

	game.generateShipLocations();
}

function handleClickButton() {
	var guessInput = document.getElementById('guess');
	var guess = guessInput.value;
	
	controller.processGuess(guess);

	guess.value = "";
}

function handleKeyPress(e) {
	var buttonValue = document.getElementById('button');
	if (e.keyCode === 13) {
		buttonValue.click();
		return false;
	}
}

window.onload = init();

var view = {
	outputMessage : 	function(msg) { // ouput the message at left up angle
								document.getElementById("message").innerHTML = msg;
							},

	outputImage : 		function(guess) { //	if user guess then output the Hit image or output the Miss image
								var getpic = document.getElementById(guess); 

								for (var i = 0; i < 3; i++) {
									for (var j = 0; j < 3; j++) {
										if(guess == game.ships[i].locations[j]) { 
											game.ships[i].hits[j] = "hit";
											return getpic.setAttribute("class", "hit"); 
										} else { getpic.setAttribute("class", "miss"); }
									}
								}
							}
};





//////////////////////////////////////////////////////////////////////////////////
////////////////////////////       TEST AREA       ///////////////////////////////
//////////////////////////////////////////////////////////////////////////////////


// controller.processGuess("A0");
// controller.processGuess("A6");
// controller.processGuess("B6");
// controller.processGuess("C6");
// controller.processGuess("C4");
// controller.processGuess("D4");
// controller.processGuess("E4");
// controller.processGuess("B0");
// controller.processGuess("B1");
// controller.processGuess("B2");


// console.log(parseGuess("A6"));
// console.log(parseGuess("J4"));
// console.log(parseGuess("B2"));
// console.log(parseGuess("T3"));
// console.log(parseGuess("A0"));
// console.log(parseGuess("C5"));
// console.log(parseGuess("D2"));


// console.log(ships[0].hits[0]);

// game.fire("06");
// game.fire("13");
// game.fire("24");
// game.fire("31");
// game.fire("34");
// game.fire("50");
// game.fire("01");
// game.fire("26");
// game.fire("10");
// game.fire("11");
// game.fire("12");
// game.fire("44");
// game.fire("16");

// console.log(game.ships);